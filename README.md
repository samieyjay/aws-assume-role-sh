# aws-assume-role-sh

Assume role script to assume roles into multiple AWS accounts 

### Usage

The script executes with names that are easy to remember for your aws accounts, and mfa code. 

```
. /path/to/assume-tole.sh dev 123456
```

### Required Values

Supply values to the following variables on the script to execute the script.

The env_acct variable is an array that can accept as many environment/aws accounts. It uses even index values as environment name while it uses odd index values for aws account numbers.

```
env=$1
mfa=$2
duration=43200 # session duration. Adjust as allowed
aws_profile=default # must be configured first with -- aws configure
aws_role=xxx # role to assume
env_mgmt_acct=xxxxxxxxxxxx # directory account
user=xxx # username
region=eu-west-2 # aws region
env_acct=('dev' 'xxxaws-accountxxx' \
          'nonprod' 'xxxaws-accountxxx')
```

Current number of position parameters is 2, if you wish to pass more such as aws_role=$3, remember to change line 38 to your number of position parameters.

```
...
if [[ -n "${account}" && "${#mfa}" -eq 6 && "${#@}" -eq 3 ]]; then
...
```