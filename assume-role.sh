#!/bin/bash

env=$1
mfa=$2
duration=43200 # session duration. Adjust as allowed
aws_profile=default # existing aws profile
aws_role=xxx # role to assume
env_mgmt_acct=xxxxxxxxxxxx # directory account
user=xxx # username
region=eu-west-2 # aws region
env_acct=('dev' 'xxxxxxxxxxxx' \
          'nonprod' 'xxxxxxxxxxxx')

# List all account names from env_acct
account_names() {
  delim=""
  for (( i = 0; i < "${#env_acct[@]}"; i = i + 2 )); do
    for item in "${env_acct[i]}"; do
      printf "%s" "$delim$item"
      delim=", "
    done
  done
}

i=0
while [ $i -lt ${#env_acct[@]} ]
do
  if [[ -n "${env}" && "${env}" == "${env_acct[i]}" ]]; then
    environment="${env_acct[i]}"
    account="${env_acct[$(($i + 1))]}"
    break
  else
    account=""
  fi
  i=$(($i + 2))
done

if [[ -n "${account}" && "${#mfa}" -eq 6 && "${#@}" -eq 2 ]]; then
  temp_cred=$(aws sts assume-role --role-arn arn:aws:iam::"${account}":role/"${aws_role}" \
        --serial-number arn:aws:iam::"${env_mgmt_acct}":mfa/"${user}" --token-code "${mfa}" \
        --duration-seconds "${duration}" --role-session-name assume-session --profile "${aws_profile}")

  export AWS_ACCESS_KEY_ID=$(echo "${temp_cred}" | jq .Credentials.AccessKeyId | xargs)
  export AWS_SECRET_ACCESS_KEY=$(echo "${temp_cred}" | jq .Credentials.SecretAccessKey | xargs)
  export AWS_SESSION_TOKEN=$(echo "${temp_cred}" | jq .Credentials.SessionToken | xargs)
  export AWS_DEFAULT_REGION="${region}"
  export ACCOUNT=$(aws sts get-caller-identity | jq -r .Account | xargs)

  if [ "${ACCOUNT}" == "${account}" ]; then
    echo "####### You are currently logged into "${environment}" environment: "${ACCOUNT}" #######"
    unset account
  fi
else
  cat << EndOfMessage

ERROR: Invalid Account Name or MFA Code!!!

Account Name: Cannot be empty or incorrect. Supported account names include; $(account_names) 
MFA Code: Cannot be empty or less/more than 6 characters

Example Usage: . /path/to/script.sh dev 123456

EndOfMessage
fi